package com.example_3.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.game.core.*;
import com.utils.*;


public class PolarCoordinateExample implements Screen {

    private Drop game;
    private OrthographicCamera camera;
    public ShapeRenderer shape;

    private static final float SCREEN_W = Constants.GUI_WIDTH;
    private static final float SCREEN_H = Constants.GUI_HEIGHT;    
    private Vector2 coord;

    public PolarCoordinateExample(final Drop game) {
        this.game = game;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, SCREEN_W, SCREEN_H);
        //camera.position.set(SCREEN_W / 2f, SCREEN_H / 2f, 0);
        shape = new ShapeRenderer();
        camera.update();
        coord = new Vector2();
    }

        
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        //
        
        coord.x = Gdx.app.getInput().getX();
        coord.y =SCREEN_H - Gdx.app.getInput().getY();
        
        
        
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        
        
        //Render frame rate
        //game.font.draw(game.batch, null, delta, delta)
        
        float cx = SCREEN_W / 2;
        float cy = SCREEN_H / 2;
        
        shape.begin(ShapeRenderer.ShapeType.Line);
        shape.setColor(com.badlogic.gdx.graphics.Color.GRAY);
        shape.line(0, cy, SCREEN_W, cy);
        shape.line(cx, 0, cx, SCREEN_H);
        shape.setColor(com.badlogic.gdx.graphics.Color.GREEN);
        shape.line(cx, cy, coord.x, coord.y);
        shape.end();
        float px = coord.x - cx;
        float py = coord.y -cy;
        float r = (float)Math.sqrt(px * px + py * py);
        float rad = (float) Math.atan2(py, px);
        float degrees = (float) Math.toDegrees(rad);
        
        if (degrees < 0) {
            degrees = 360 + degrees;
        }
        double sx = r * Math.cos(rad);
        double sy = r * Math.sin(rad);
        String polar = String.format("(%.0f,%.0f\u00b0)", r, degrees);
        game.batch.begin();
        game.font.draw(game.batch,polar, 20, 60);
        String cart = String.format("(%.0f,%.0f)", sx, sy);
        game.font.setColor(com.badlogic.gdx.graphics.Color.WHITE);
        game.font.draw(game.batch, cart, 20, 80);
        game.font.draw(game.batch,String.format("(%s,%s)", px, py), coord.x, coord.y);
        game.font.setColor(com.badlogic.gdx.graphics.Color.BLUE);
        game.batch.end();
        shape.begin(ShapeRenderer.ShapeType.Line);
        
        shape.arc( cx , cy , r,   0,   degrees);
        shape.end();
        
        
        if(Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)){
            game.setScreen(new MainMenuScreen(game));
           dispose();
        }
        
    }

    @Override
    public void show() {

    }

    

    @Override
    public void resize(int i, int i1) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        shape.dispose();
        game.font.setColor(com.badlogic.gdx.graphics.Color.WHITE);
    }
}
