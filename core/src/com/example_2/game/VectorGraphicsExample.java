/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example_2.game;

import com.game.core.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.utils.VectorC2;
import com.badlogic.gdx.graphics.Color;

/**
 *
 * @author reef
 */
public class VectorGraphicsExample implements Screen {

    private Drop game;
    private OrthographicCamera camera;
    private ShapeRenderer shape;
    
    
    
    private VectorC2 pl;
    private static final float SCREEN_W = com.utils.Constants.GUI_WIDTH;
    private static final float SCREEN_H = com.utils.Constants.GUI_HEIGHT;
    private VectorC2[] polygon;
    private VectorC2[] world;
    private float tx, ty;
    private float vx, vy;
    private float rot, rotStep;
    private float scale, scaleStep;
    private float sx, sxStep;
    private float sy, syStep;
    private boolean doTranslate;
    private boolean doScale;
    private boolean doRotate;
    private boolean doXShear;
    private boolean doYShear;

    public VectorGraphicsExample() {
    }
    
    public VectorGraphicsExample(final Drop game) {
        this.game = game;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, SCREEN_W, SCREEN_H);
        camera.position.set(SCREEN_W / 2f, SCREEN_H / 2f, 0);
        shape = new ShapeRenderer();
        camera.update();
        initialize();

    }

    private void initialize() {
        polygon = new VectorC2[]{new VectorC2(10, 0),
            new VectorC2(-10, 8),
            new VectorC2(0, 0),
            new VectorC2(-10, -8),};
        world = new VectorC2[polygon.length];
        reset();
    }

    private void reset() {
        tx = SCREEN_W / 2;
        ty = SCREEN_H / 2;
        vx = vy = 2;
        rot = 0.0f;
        rotStep = (float) Math.toRadians(1.0);
        scale = 1.0f;
        scaleStep = 0.1f;
        sx = sy = 0.0f;
        sxStep = syStep = 0.01f;
        doRotate = doScale = doTranslate = false;
        doXShear = doYShear = false;
    }

    private void processInput() {

        if (Gdx.input.isKeyJustPressed(Keys.R)) {
            doRotate = !doRotate;
        }
        //S
        if (Gdx.input.isKeyJustPressed(Keys.S)) {
            doScale = !doScale;
        }
        //T
        if (Gdx.input.isKeyJustPressed(Keys.T)) {
            doTranslate = !doTranslate;
        }
        //X
        if (Gdx.input.isKeyJustPressed(Keys.X)) {
            doXShear = !doXShear;
        }
        //Y
        if (Gdx.input.isKeyJustPressed(Keys.Y)) {
            doYShear = !doYShear;
        }
        //SPACE
        if (Gdx.input.isKeyJustPressed(Keys.SPACE)) {
            reset();
        }
        if(Gdx.input.isKeyJustPressed(Keys.ESCAPE)){
            game.setScreen(new MainMenuScreen(game));
           dispose();
        }

    }

    private void processObjects() {
        // copy data...
        for (int i = 0; i < polygon.length; ++i) {
            world[i] = new VectorC2(polygon[i]);
        }
        if (doScale) {
            scale += scaleStep;
            if (scale < 1.0 || scale > 5.0) {
                scaleStep = -scaleStep;
            }
        }
        if (doRotate) {
            rot += rotStep;
            if (rot < 0.0f || rot > 2 * Math.PI) {
                rotStep = -rotStep;
            }
        }
        if (doTranslate) {
            tx += vx;
            if (tx < 0 || tx > SCREEN_W) {
                vx = -vx;
            }
            ty += vy;
            if (ty < 0 || ty > SCREEN_H) {
                vy = -vy;
            }
        }
        if (doXShear) {
            sx += sxStep;
            if (Math.abs(sx) > 2.0) {
                sxStep = -sxStep;
            }
        }
        if (doYShear) {
            sy += syStep;
            if (Math.abs(sy) > 2.0) {
                syStep = -syStep;
            }
        }
        for (int i = 0; i < world.length; ++i) {
            world[i].shear(sx, sy);
            world[i].scl(scale, scale);
            world[i].rotateC(rot);
            world[i].translate(tx, ty);
        }
    }


    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.font.draw(game.batch, "Translate(T): " + doTranslate, 20, 35);
        game.font.draw(game.batch, "Rotate(R) : " + doRotate, 20, 50);
        game.font.draw(game.batch, "Scale(S) : " + doScale, 20, 65);
        game.font.draw(game.batch, "X-Shear(X) : " + doXShear, 20, 80);
        game.font.draw(game.batch, "Y-Shear(Y) : " + doYShear, 20, 95);
        game.font.draw(game.batch, "Press [SPACE] to reset", 20, 110);
        game.batch.end();

        processInput();
        processObjects();
        VectorC2 S = world[world.length - 1];
        VectorC2 P = null;

        shape.begin(ShapeRenderer.ShapeType.Line);
        //  shape.setProjectionMatrix(camera.combined);
        shape.setColor(Color.GREEN);
        for (int i = 0; i < world.length; ++i) {
            P = world[i];
            shape.line((int) S.x, (int) S.y, (int) P.x, (int) P.y);
            S = P;
        }
        shape.end();
    }


    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {
//         float aspectRatio = (float) width / (float) height;
//        camera = new OrthographicCamera(2f * aspectRatio, 2f);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
    //    game.dispose();
    }
}
