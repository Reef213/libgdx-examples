/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bucket.game;

import com.game.core.Drop;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.TimeUtils;
import com.game.core.MainMenuScreen;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author reef
 */
public class GameScreen implements Screen {

    final Drop game;

    SpriteBatch batch;
    Texture dropImg;
    Texture bucketImg;
    Sound dropSound;
    Music rainMusic;
    private ArrayList<Rectangle> raindrops;
    private long lastDropTime;
    private OrthographicCamera camera;
    private Rectangle bucket;

    int dropsGathered;

    public GameScreen(Drop game) {
        this.game = game;

        dropImg = new Texture(Gdx.files.internal("droplet.png"));
        bucketImg = new Texture(Gdx.files.internal("bucket.png"));

        dropSound = Gdx.audio.newSound(Gdx.files.internal("drips9.wav"));
        rainMusic = Gdx.audio.newMusic(Gdx.files.internal("rain.mp3"));

              // rainMusic.setLooping(true);
        //rainMusic.play();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, com.utils.Constants.GUI_WIDTH, com.utils.Constants.GUI_HEIGHT);
              //  batch=new SpriteBatch();

        bucket = new Rectangle();
        bucket.x = com.utils.Constants.GUI_WIDTH / 2 - 64 / 2;
        bucket.y = 20;
        bucket.width = 64;
        bucket.height = 64;

        raindrops = new ArrayList<Rectangle>();
        spawnRaindrop();
    }

    @Override
    public void show() {

    }

    private void processInput() {
        if (Gdx.input.isButtonPressed(Input.Keys.BUTTON_L1)) {
            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            bucket.x = touchPos.x - 64 / 2;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            bucket.x -= 200 * Gdx.graphics.getDeltaTime();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            bucket.x += 200 * Gdx.graphics.getDeltaTime();
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.Q)) {
            Gdx.app.exit();
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            game.setScreen(new MainMenuScreen(game));
            dispose();
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.font.draw(game.batch, "Count=" + dropsGathered, 50, com.utils.Constants.GUI_HEIGHT - 50);
        game.batch.draw(bucketImg, bucket.x, bucket.y);

        for (Rectangle raindrop : raindrops) {
            game.batch.draw(dropImg, raindrop.x, raindrop.y);

        }
        game.batch.end();
        
        processInput();
        
        if (bucket.x < 0) {
            bucket.x = 0;
        }
        if (bucket.x > com.utils.Constants.GUI_WIDTH - 64) {
            bucket.x = com.utils.Constants.GUI_WIDTH - 64;
        }

        if (TimeUtils.nanoTime() - lastDropTime > 1000000000) {
            spawnRaindrop();
        }

        Iterator<Rectangle> iter = raindrops.iterator();
        while (iter.hasNext()) {
            Rectangle raindrop = iter.next();
            raindrop.y -= 200 * Gdx.graphics.getDeltaTime();
            if (raindrop.y + 64 < 0) {
                iter.remove();
            }
            if (raindrop.overlaps(bucket)) {
                dropsGathered++;
                dropSound.play();
                iter.remove();
            }
        }

    }

    @Override
    public void resize(int i, int i1) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        dropImg.dispose();
        bucketImg.dispose();
        dropSound.dispose();
        rainMusic.dispose();
       // game.dispose();
    }

    private void spawnRaindrop() {
        Rectangle raindrop = new Rectangle();
        raindrop.x = MathUtils.random(0, com.utils.Constants.GUI_HEIGHT - 64);
        raindrop.y = com.utils.Constants.GUI_WIDTH;
        raindrop.width = 64;
        raindrop.height = 64;
        raindrops.add(raindrop);
        lastDropTime = TimeUtils.nanoTime();
    }

}
