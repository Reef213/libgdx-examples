/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.game.core;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;



/**
 *
 * @author reef
 */
public class Drop extends Game {
    public SpriteBatch batch;
    public BitmapFont font;
    
    
    @Override
    public void create() {
        batch=new SpriteBatch();
        font=new BitmapFont();
        this.setScreen(new MainMenuScreen(this));
    }
    
    public void render(){
        super.render();
    }
    
    public void dispose(){
        batch.dispose();
        font.dispose();
    }
}
