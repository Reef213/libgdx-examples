/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.game.core;

import com.bucket.game.*;
import com.example_2.game.VectorGraphicsExample;
import com.example_3.game.PolarCoordinateExample;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.exmaple_4.game.MatrixMultiplyExample;


/**
 *
 * @author reef
 */
public class MainMenuScreen implements Screen {

    final Drop game;
    private final OrthographicCamera camera;
    private final Rectangle rectangle;
    
    public MainMenuScreen(final Drop game) {
        this.game=game;
        camera=new OrthographicCamera();
        camera.setToOrtho(false, com.utils.Constants.GUI_WIDTH, com.utils.Constants.GUI_HEIGHT);
        rectangle=new Rectangle();
        
    }

    @Override
    public void show() {
       
    }

    @Override
    public void render(float delta) {
       Gdx.gl.glClearColor(0, 0, 0.2f, 1);
       Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
       
       camera.update();
       game.batch.setProjectionMatrix(camera.combined);
       
       game.batch.begin();
       game.font.draw(game.batch, "Press 1 to run Bucket Game", 100, com.utils.Constants.GUI_HEIGHT-100);
       game.font.draw(game.batch, "Press 2 to run Example_2 game", 100, com.utils.Constants.GUI_HEIGHT-150);
       game.font.draw(game.batch, "Press 3 to run Example_3 game", 100, com.utils.Constants.GUI_HEIGHT-200);
       game.font.draw(game.batch, "Press 4 to run Example_4 game", 100, com.utils.Constants.GUI_HEIGHT-250);
       game.batch.end();
       
//       if(Gdx.input.isTouched()){
//           game.setScreen(new GameScreen(game));
//           dispose();
//       }
       if(Gdx.input.isKeyPressed(Keys.NUM_1)){
           game.setScreen(new GameScreen(game));
           dispose();
       }
       
       if(Gdx.input.isKeyPressed(Keys.NUM_2)){
           game.setScreen(new VectorGraphicsExample(game));
           dispose();
       }
       
       if(Gdx.input.isKeyPressed(Keys.NUM_3)){
           game.setScreen(new PolarCoordinateExample(game));
           dispose();
       }
       
       if(Gdx.input.isKeyPressed(Keys.NUM_4)){
           game.setScreen(new MatrixMultiplyExample(game));
           dispose();
       }
       
       if(Gdx.input.isKeyPressed(Keys.Q)){

           Gdx.app.exit();
           dispose();
       }
    }

    @Override
    public void resize(int i, int i1) {
       
    }

    @Override
    public void pause() {
       
    }

    @Override
    public void resume() {
       
    }

    @Override
    public void hide() {
       
    }

    @Override
    public void dispose() {
       
    }
    
}
