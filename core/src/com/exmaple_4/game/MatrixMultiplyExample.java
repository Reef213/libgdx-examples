package com.exmaple_4.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Vector2;

import com.game.core.Drop;
import com.game.core.MainMenuScreen;
import com.utils.Constants;

public class MatrixMultiplyExample implements Screen {

    private Drop game;
    private OrthographicCamera camera;
    public ShapeRenderer shape;

    private static final float SCREEN_W = Constants.GUI_WIDTH;
    private static final float SCREEN_H = Constants.GUI_HEIGHT;
    private float earthRot, earthDelta;
    private float moonRot, moonDelta;
    private Matrix3 sunMat;
    private Matrix3 earthMat;
    private Matrix3 zeta;
    private Matrix3 moonMat;
    private Vector2 sun;
    private Vector2 earth ;
    private Vector2 moon;

    public MatrixMultiplyExample(Drop game) {
        this.game = game;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, SCREEN_W, SCREEN_H);
        camera.position.set(SCREEN_W / 2f, SCREEN_H / 2f, 0);
        shape = new ShapeRenderer();
        camera.update();
        initialize();
    }

    private void initialize() {
        earthDelta = (float) Math.toRadians(0.5);
        moonDelta = (float) Math.toRadians(2.5);
        sunMat = new Matrix3();
        earthMat = new Matrix3();
        zeta = new Matrix3();
        moonMat = new Matrix3();
      
        
    }

    private void processInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.Q)) {
            Gdx.app.exit();
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            game.setScreen(new MainMenuScreen(game));
            dispose();
        }
    }

    @Override
    public void render(float delta) {
        processInput();
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.font.setColor(com.badlogic.gdx.graphics.Color.GREEN);
        game.font.draw(game.batch, "Press [ESC] to Back", 20, 35);
        game.font.draw(game.batch, "Press [Q] to Exit", 20, 15);
        game.batch.end();
        shape.begin(ShapeRenderer.ShapeType.Filled);
        // draw the Sun       
        shape.setColor(com.badlogic.gdx.graphics.Color.YELLOW);
        shape.ellipse(SCREEN_W / 2 - 50, SCREEN_H / 2 - 50, 100, 100);
        shape.end();
        shape.begin(ShapeRenderer.ShapeType.Line);
        // draw Earth's Orbit
        shape.setColor(com.badlogic.gdx.graphics.Color.WHITE);
        shape.circle(SCREEN_W / 2, SCREEN_H / 2, SCREEN_W / 4 + 40);
        shape.end();
        //
        updateMatrix(sunMat,earthMat,SCREEN_W / 4, SCREEN_H / 4,SCREEN_W / 2, SCREEN_H / 2,earthRot);
        earthRot += earthDelta;
        earth = earthMat.getTranslation(new Vector2());
        shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.setColor(com.badlogic.gdx.graphics.Color.BLUE);
        shape.ellipse(earth.x - 10, earth.y - 10, 20, 20);
         shape.end();
        updateMatrix(zeta,moonMat,30,0,earth.x,earth.y,moonRot);
        //
        moonRot += moonDelta;
        moon = moonMat.getTranslation(new Vector2());
        shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.setColor(com.badlogic.gdx.graphics.Color.LIGHT_GRAY);
        shape.ellipse(moon.x - 5, moon.y - 5, 10, 10);
        shape.end();
    }
    /**
     * 
     * @param prevM
     * @param secM
     * @param prX
     * @param prY
     * @param scX
     * @param scY
     * @param degrees угол поворота
     * @return 
     */
    private Matrix3 updateMatrix(Matrix3 prevM,Matrix3 secM,
                                float prX,float prY,float scX,float scY,
                                float degrees){
        prevM.idt();
        //prX, prY Задается радиус вращения
        prevM.translate(prX, prY);
        secM.idt();
        //scX,scY Задается центр вращения
        secM.translate(scX,scY);
        secM.rotateRad(degrees);
        secM.mul(prevM);
        return secM;
    }

    @Override
    public void show() {

    }

    @Override
    public void resize(int i, int i1) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        game.font.setColor(com.badlogic.gdx.graphics.Color.CLEAR);
        //   shape.dispose();
    }
}
