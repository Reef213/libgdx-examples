/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils;

import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author reef
 */
public class VectorC2 extends Vector2 {
    public VectorC2(){};
    public VectorC2(Vector2 v){
        super.add(v);
    };
    public VectorC2(float x,float y){
        super.x=x;
        super.y=y;
    }
    
    public void rotateC(float rad) {
        float tmp = (float) (super.x * Math.cos(rad) - super.y * Math.sin(rad));
        super.y = (float) (super.x * Math.sin(rad) + super.y * Math.cos(rad));
        super.x = tmp;
    }

    public void translate( float tx, float ty) {
        super.x += tx;
        super.y += ty;

    }

    public void shear(float sx, float sy) {
        float tmp = super.x + sx * super.y;
        super.y = super.y + sy * super.x;
        super.x = tmp;
    }
}
