/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils;

import java.io.File;

/**
 *
 * @author reef
 */
public class Constants {
    
    /**
     * GUI WIDTH 
     */
    public static final float GUI_WIDTH=640;
    
    /**
     * GUI HEIGHT
     */
    public static final float GUI_HEIGHT=480;
    
    /**Files location
     * 
     */
    public static final String TEXTURE_PITER="images/piter_sprite.png";
    public static final String TEXTURE_SHIP_XWING="images"+File.separator+"ship_xwing.png";
    public static final String TEXTURE_SHIP_COOL="images/ship_cool.png";
    public static final String TEXTURE_SHIP_WAR="images/ship_war.png";
    public static final String TEXTURE_METEORS="images"+File.separator+"meteors.png";
    /**
     * file settings
     */
    public static final String PREFERNCES="game.prefs";
    
}
